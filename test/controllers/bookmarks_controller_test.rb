require 'test_helper'

class BookmarksControllerTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end

  test 'bookmarks actions require user login' do
    get '/bookmarks'
    assert_not_nil json_response['errors']
  end

  test 'users can create bookmarks' do
    post '/bookmarks',params:{ bookmark: {url:'http://google.com', tags:['search_engine','google']} },headers:auth_headers('gokay@gmail.com')
    assert_not_nil json_response['url']
  end

  test 'users can list bookmarks' do
    get '/bookmarks',headers:auth_headers('gokay@gmail.com')
    assert_not_nil json_response['bookmarks']
  end

  test 'users can update bookmarks' do
    bookmark = User.find_by(email:'gokay@gmail.com').bookmarks.create(url:'http://reddit.com',tags:['reddit','waste_of_time'])
    put "/bookmarks/#{bookmark.id}",params:{bookmark:{url:'http://reddit2.com'}},headers:auth_headers('gokay@gmail.com')
    bookmark.reload
    assert 'http://reddit2.com', bookmark.url
  end

  test 'users can delete bookmark' do
    bookmark = User.find_by(email:'gokay@gmail.com').bookmarks.create(url:'http://reddit.com',tags:['reddit','waste_of_time'])
    delete "/bookmarks/#{bookmark.id}",headers:auth_headers('gokay@gmail.com')
    assert_nil Bookmark.find_by(id:bookmark.id)
  end

  test 'users are not able to modify bookmarks of others' do
    bookmark = User.find_by(email:'gokay2@gmail.com').bookmarks.create(url:'http://reddit.com',tags:['reddit','waste_of_time'])
    delete "/bookmarks/#{bookmark.id}",headers:auth_headers('gokay@gmail.com')
    assert_not_nil json_response['errors']
  end
end
