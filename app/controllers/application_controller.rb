class ApplicationController < ActionController::API
  include DeviseTokenAuth::Concerns::SetUserByToken

  def error_envelope(msg)
    { 'errors': msg }
  end

  def check_for_user
    unless current_user
      return render json: error_envelope('session_error')
    end
  end
end
