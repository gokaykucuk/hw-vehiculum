class BookmarksController < ApplicationController

  before_action :check_for_user

  def index
    render json: {bookmarks:current_user.bookmarks}
  end

  def create
    bookmark = current_user.bookmarks.create(params_bookmark)
    render json: bookmark
  #rescue StandardError => e
  #  render 500, json: error_envelope('generic_error')
  end

  def update
    bookmark = current_user.bookmarks.find(params[:id])
    bookmark.assign_attributes(params_bookmark)
    bookmark.save
    render json: bookmark
  rescue StandardError => e
    render json: error_envelope('generic_error')
  end

  def destroy
    bookmark = current_user.bookmarks.find(params[:id])
    bookmark.destroy
    render json: true
  rescue StandardError => e
    render json: error_envelope('generic_error')
  end


  def redirect_short_url
    redirect_to Shortlink.find_by(short_url:params[:short_url]).url
  end


  private

  def params_bookmark
    params.require(:bookmark).permit(:url,tags:[])
  end


end
