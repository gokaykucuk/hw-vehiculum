class Bookmark < ApplicationRecord
  before_validation :sync_hosts_and_shortlinks

  belongs_to :host
  belongs_to :user

  def as_json(_options={})
    {
        url:url,
        short_link: Shortlink.find_by(url:url).short_url,
        tags:tags
    }
  end

  private
  def sync_hosts_and_shortlinks
    # Create and assign the host model
    host_address = URI.parse(url).host
    self.host = Host.find_or_create_by(host:host_address)
    # Create or check if it's there for short_link
    Shortlink.find_or_create_by(url:url)
  end


end
