class Shortlink < ApplicationRecord
  before_validation :create_shortlink

  private
  def create_shortlink
    self.short_url = rand(36**5).to_s(36)
  end
end
