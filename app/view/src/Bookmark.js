import React, { Component } from 'react';

class Bookmark extends Component {

    constructor(props) {
        super(props);
        this.state = {
            url: props['url'],
            short_url: props['short_url'],
            tags: props['tags']
        };
    }

    render() {
        const { url, short_url, tags } = this.state;
        return (
            <div className="bookmark">
                <a href={short_url}>
                    <h3 className="bookmark-header">{{ url }}</h3>
                    <p>{{url}}</p>
                    <ul>
                        {tags.map(tag=>(
                                <li>tag</li>
                            )
                        )}
                    </ul>
                </a>

            </div>
        );
    }
}

export default Bookmark;
