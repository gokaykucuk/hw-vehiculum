import React, {Component} from 'react';
import Auth from 'j-toker';
import logo from './logo.svg';
import * as $ from 'jquery';
import './App.css';

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loggedInUser: null,
            bookmarks: [],
            bookmarksToRender:[], //This array is going to be rendered depending on search criteria
            addBookmarkUrl: '',
            email: '',
            password: ''
        };

        Auth.configure({
            apiUrl: 'http://localhost:3000'
        });

        this.login=this.login.bind(this);
        this.signup = this.signup.bind(this);
        this.pullBookmarks = this.pullBookmarks.bind(this);
        this.handleFilter = this.handleFilter.bind(this);

    }

    handleFilter(event) {
        const { bookmarks } = this.state;
        let search_param = event.target.value;
        if( search_param === ""){
            this.setState({bookmarksToRender: bookmarks});
            return true;
        }
        // Filter out the unwanted items.
        let filtered_bookmarks = bookmarks.filter(function(bookmark){
            return bookmark.url.includes(search_param) || bookmark.tags.join().includes(search_param);
        });
        this.setState({bookmarksToRender:filtered_bookmarks});
    }

    login(){
        let {email, password} = this.state;
        let self = this;
        Auth.emailSignIn({
            email:email,
            password:password
        }).then(function(user) {
            console.log(user);
            self.setState({
                loggedInUser: user
            });
            self.pullBookmarks();
        }.bind(this))
            .fail(function() {
                console.log('Username password error');
                self.signup();
                self.setState({
                    loggedInUser: null
                })
            });
    }

    signup(){
        let {email, password} = this.state;
        let self = this;
        Auth.emailSignUp({
            email: email,
            password: password,
            password_confirmation: password
        }).then(function(user) {
                // After signing the user up, just call the login function with same creds.
                self.login();
        }.bind(this))
            .fail(function() {
                alert('Something went wrong, please try again.');
            });
    }

    componentDidMount(){
        let self = this;
        Auth.validateToken()
            .then(function(user) {
                self.setState({
                    loggedInUser: user
                });
                self.pullBookmarks();
            }.bind(this))
            .fail(function() {
                self.setState({
                    loggedInUser: null
                })
            });
    }

    createBookmark(e){
        let self = this;
        let {addBookmarkUrl, addBookmarkTags} = this.state;
        console.log(addBookmarkUrl);
        $.ajax({
            type: "POST",
            url: "http://localhost:3000/bookmarks",
            contentType: "application/json",
            data: JSON.stringify({
                "bookmark":{
                    "url":addBookmarkUrl,
                    "tags": addBookmarkTags
                }
            }),
            success: function (data) {
                console.log(data);
                self.pullBookmarks();
            }
        });
    }

    pullBookmarks(){
        let self = this;
        $.get("http://localhost:3000/bookmarks",function(data){
            console.log(data.bookmarks);
            self.setState({bookmarks:data.bookmarks,bookmarksToRender:data.bookmarks});
        });

    }

    updateEmail(e){
        this.setState({
            email: e.target.value
        })
    }
    updatePassword(e){
        this.setState({
            password: e.target.value
        })
    }

    updateBookmarkUrl(e){
        this.setState({
            addBookmarkUrl:e.target.value
        })
    }
    updateBookmarkTags(e){
        this.setState({
            addBookmarkTags:e.target.value.split(',')
        })
    }

    render() {
        let {loggedInUser, bookmarksToRender} = this.state;

        let loginPartial = (<div>
            <div style={{textAlign:'center'}}>
                <p>Please login or signup using the following form</p>
                <label>Email</label>
                <br/>
                <input id={'username'} type="text" name="email" value={this.state.email} onChange={e => this.updateEmail(e)}/>
                <br/>
                <label>Password</label>
                <br/>
                <input id={'password'} type="password" name="password" value={this.state.password} onChange={e => this.updatePassword(e)}/>
                <br/><br/>
                <button onClick={this.login}>Log in or Signup</button>
            </div>

        </div>);

        if (loggedInUser===null){
            return loginPartial;
        }

        return (
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo"/>
                    <h1 className="App-title">Awesome Bookmarker</h1>
                </header>
                <div className="action-box-container">
                    <div className="action-box">
                        <div style={{textAlign:'center'}}>
                            <h1>Add new Bookmark</h1>
                            <label>URL</label><br/>
                            <input type='text' name='urlinput' value={this.state.addBookmarkUrl} onChange={e => this.updateBookmarkUrl(e)}/><br/>
                            <label>Tags(Seperate tags with comma)</label><br/>
                            <input type='text' name='tagsinput' value={this.state.addBookmarkTags} onChange={e => this.updateBookmarkTags(e)}/><br/>
                            <br/>
                            <button onClick={ e => this.createBookmark(e) }>Add</button>
                        </div>
                    </div>
                    <div className="action-box">
                        <div style={{textAlign:'center'}}>
                            <h1>Filter Bookmarks</h1>
                            <label>Search</label><br/>
                            <input type='text' name='searchinput' onChange={e => this.handleFilter(e)}/><br/>
                            <br/>
                        </div>
                    </div>
                </div>
                <div className="clearfix"></div>
                <h3>Bookmarks</h3>
                <ul>
                    {bookmarksToRender.map(bookmark => (
                        <li key={bookmark.id}>
                            {bookmark.url}
                            <ul>
                                {bookmark.tags && bookmark.tags.map(tag=> (
                                    <li key={bookmark.id+tag}>{tag}</li>

                                ))}
                            </ul>


                        </li>
                    ))}
                </ul>

            </div>
        );
    }
}

export default App;
