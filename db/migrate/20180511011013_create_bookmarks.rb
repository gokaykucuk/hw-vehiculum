class CreateBookmarks < ActiveRecord::Migration[5.2]
  def change
    create_table :bookmarks do |t|
      t.string :url
      t.jsonb :tags, default:[]
      t.references :user
      t.references :host
      t.timestamps
    end
  end
end
