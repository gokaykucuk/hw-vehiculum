class CreateShortlinks < ActiveRecord::Migration[5.2]
  def change
    create_table :shortlinks do |t|
      t.string :short_url, index: true, unique: true
      t.string :url, index: true

      t.timestamps
    end
  end
end
