Rails.application.routes.draw do
  mount_devise_token_auth_for 'User', at: 'auth'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :bookmarks
  get '/r/:short_url', to: 'bookmarks#redirect_short_url', as: :short_redirect
end
