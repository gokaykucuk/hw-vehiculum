# Vehiculum Project

The project at the hand is a bookmarking application with Ruby on Rails.

# Data Models

The projects database requirements can be easily satisfied by **PostgreSQL**, also tagging system would be perfect for using **JSON** capabities of PostgreSQL.

## User

A bookmarking application should be able to do standart user management. A bookmark entry in the database will always belong to a **user**. While the user management on this project is very trivial at this stage, an external library like **devise_token_auth** will be used for future extebsibility.

### Columns

- username : string
- password : string
- email : string

## Host

A bookmark entry will also belong to a **host**. Whenever a bookmark gets **saved or updated**, host information will be extracted from bookmark url and a host entry in the database will be created. Host entries will **not** belong to a user nor to a bookmark.

### Columns

- host : string

## Bookmark

In the context of our project, bookmarks are at the core. A bookmark item can be created modified or deleted by its own user. Whenever a user creates a bookmark, the bookmark will extract its host, check whether it can find a host entry, if not create a new one, then assign itself to that host before saving. For tagging purposes, bookmark entries will have a **JSONB** column.
The links for bookmarks also will be saved fully, the idea of not saving the **host** part of the link, and creating the entire link on the fly by combining information from hosts table, is not a good one due to price of storage vs price of computation cycles.

### Columns

- url : string
- tags : JSONB
- user_id : reference
- host_id : reference

## Shortlink

Shortlinks are going to be implemented without any **dependency** to the rest of the application to make them easier to extract into another self-contained environment, in the event of moving our system to microservice based structure. They are not a fundemental part of the context and also their implementation can be easly done in self-contained manner. Shortlinks are going to be **created** whenever a user creates a bookmark with a link **which doesn't have** a shortlink before, two users bookmarking the same url will create only 1 entry in the database. Corresponding shortlinks table will have indexes on both short_url links and long_url links. Querying from both vectors(short_url or long_url) should be responsive.

### Columns

- short_url : string
- url : string

# Frontend

A frontend will be created using **React/Redux** and **Bootstrap**. Which would include CRUD functionality with basic user management.
A generic **browser extension** is the most logical mean of distribution, so a simple browser extension will be another way of deployment.

# Project Plan

Here is the list of subtasks that needs to be completed. This project is going to be developed by a single developer,  so the subtasks created serial working in mind(one task after another). Backend will be created using api_only mode of rails for better performance.

- Write failing tests for business layer(~1 hour)
- Generation of models and controllers and implementation of business logic to satisfy the tests(~1 hour)
- Creation of ci integration for automated testing and deployment (~0.5 hour)
- Create a Dockerfile for deployable server container images.(~0.5 hour)
- Create docker compose script for easier onboarding for development(~0.5 hour)
- Learn React+Redux(~2 hours)
- Creation a single page frontend(~1,5 hours)
- Creation of a generic browser extension(~1 hour)
- Implementation of low level caches for faster responses(~0.5 hour)
- Some refactoring(~1 hour)

Total project time will be around 10 hours.

# Deployment

For deployment, artifacts of browser extensions will be made available for download through **downloads or releases** pages of git service that's been used, and backend will be deployed on **heroku** by CI tool.