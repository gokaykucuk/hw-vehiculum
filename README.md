# README

## Installation

Standard Rails installation, I've used Ruby 2.5.1 but it should be ok with 2.4.4 too. But
you might need to update Gemfile because it includes ruby versions now.
After running 'bundle install', you also need the following,

- Postgresql server (defaults can be found and configured in database.yml)

### Docker-Compose

If you have docker and docker-compose installed, you can just run,
- docker-compose up

And then you should be able to access the app through localhost:3000 

### IMPORTANT
Dont' forget to run migrations, otherwise React frontend will generate weird errors.

## React

In this application I tried to make the frontend with React. To achive this,
the project started as a **Rails API** project, app/views folder are empty, instead there
is a React application living in **app/view**. When built react application will deploy itself
into /public folder if the folder is cleaned.

To start react development server, you can navigate into app/view folder then run
- npm start

## View

The view is a single page application. It pulls all the information about bookmarks on boot, 
and then the search functionality runs on client side.
