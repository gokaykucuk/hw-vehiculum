FROM ruby:2.5.1-alpine3.7

ENV BUILD_PACKAGES="curl-dev ruby-dev build-base bash git" \
    DEV_PACKAGES="zlib-dev libxml2-dev libxslt-dev tzdata yaml-dev postgresql-dev mysql-dev" \
    RUBY_PACKAGES="ruby-json yaml nodejs"

# Update and install base packages and nokogiri gem that requires a
# native compilation
RUN apk update && \
    apk upgrade && \
    apk add --update\
    $BUILD_PACKAGES \
    $DEV_PACKAGES \
    $RUBY_PACKAGES && \
    rm -rf /var/cache/apk/* && \
    mkdir -p /var/app

ADD . /var/app
WORKDIR /var/app
RUN bundle config build.nokogiri --use-system-libraries && \
            bundle install && \
            bundle clean
EXPOSE 3000
ENTRYPOINT /var/app/migrate_run.sh