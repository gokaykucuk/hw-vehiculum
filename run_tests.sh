#!/usr/bin/env sh
export BUILD_PACKAGES="curl-dev ruby-dev build-base bash git"
export DEV_PACKAGES="zlib-dev libxml2-dev libxslt-dev tzdata yaml-dev postgresql-dev mysql-dev"
export RUBY_PACKAGES="ruby-json yaml nodejs"
export DB_HOST="postgres"

apk update
apk upgrade
apk add --update $BUILD_PACKAGES $DEV_PACKAGES $RUBY_PACKAGES
rm -rf /var/cache/apk/*

bundle install
rake db:create
RAILS_ENV=test rake db:migrate
rake test

